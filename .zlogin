# if running from TTY1 start sway
if [ "$(tty)" = "/dev/tty1" ]; then
	#cd $HOME
	export PATH=$PATH:$HOME/.local/bin/
	export $(grep -v '^#' ~/.config/environment.d/envvars.conf | xargs -d '\n')

	if [ -e "/dev/dri/card1" ]; then
		if [ ! -z "$(udevadm info -a -n /dev/dri/card0 | grep amdgpu)" ]; then
			radeon="/dev/dri/card0"
			igpu="/dev/dri/card1"
		else
			radeon="/dev/dri/card1"
			igpu="/dev/dri/card0"
		fi
		#export WLR_RENDERER=vulkan
		log=~/tmp/sway_$(date +'%FT%H%M%S').log
		WLR_DRM_DEVICES="${radeon}:${igpu}" exec sway >> $log 2>&1
	        #WLR_DRM_DEVICES="${radeon}" exec sway >> $log 2>&1
	else # no eGPU detected
		exec sway >> ~/tmp/sway_$(date +'%FT%H%M%S').log 2>&1
	fi

	exit
fi
