#!/usr/bin/python3

import re
import subprocess
import asyncio
import swayipc


match = re.compile('<span.*>([0-9]+)</span>').match
sub = re.compile('- (Vivaldi)|(Chromium)').sub


def tree_to_list(tree):
    nodes = [tree]
    if 'nodes' in tree:
        add_nodes(tree['nodes'], nodes)
    if 'floating_nodes' in tree:
        add_nodes(tree['floating_nodes'], nodes)
    return nodes


def add_nodes(tree, nodes, output=''):
    for node in tree:
        if 'output' in node:
            output = node['output']
        node['output'] = output  # save for later
        nodes.append(node)
        if 'nodes' in node:
            add_nodes(node['nodes'], nodes, output)
        if 'floating_nodes' in node:
            add_nodes(node['floating_nodes'], nodes, output)


async def main():
    tree = await swayipc.get_tree()  # see sway-ipc(7)
    nodes = tree_to_list(tree)
    windows = []
    for node in nodes:
        window = {
            'id': node['id'],
            'name': sub('', str(node['name'])).replace('&', '&amp;'),
            'instance': '',
            'class': '',
            'output': node.get('output', ''),
        }
        if 'window_properties' in node:
            inst = node['window_properties']['instance']
            window['name'] = window['name'].replace('[mosh]', f'<span size="small" style="oblique">{inst}:</span>')
            window.update({
                'instance': inst,
                'class': node['window_properties']['class'],
            })
        elif node.get('app_id') == 'Alacritty':
            window['class'] = 'Alacritty'
        elif not node.get('nodes') and node['type'] != 'workspace':
            if node['app_id'] not in window['name']:
                window['class'] = node['app_id']
        else:
            continue
        windows.append(window)
    wofi = '\n'.join('<span size="0">{id}</span>{name}<span size="xx-small" style="oblique"> {class}</span> <span size="xx-small">{output}</span>'.format(**i) for i in windows)
    lines = len(windows)
    if lines > 16:
        lines = 16
    options = ['wofi', '--dmenu', '-i', '--lines', str(lines),
               '--prompt', 'wybierz okno', '--cache-file', '/dev/null',
               '--allow-markup']

    try:
        selection = subprocess.check_output(options, input=wofi, universal_newlines=True)
    except Exception:
        exit(0)

    selection = match(selection)
    if selection is None:
        exit(0)
    selection = selection.groups(0)[0]
    if selection.isdigit():
        response = await swayipc.run_command(f'[con_id="{selection}"] focus')
        if not response[0]['success']:
            print('cannot switch window:', response)

asyncio.run(main())
