-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup").widget
-- widget library
local vicious = require("vicious")
vicious.contrib = require("vicious.contrib")
local cal = require('cal')

-- Load Debian menu entries
require("debian.menu")

-- useful for debugging, marks the beginning of rc.lua exec
print("Entered rc.lua: " .. os.date('%Y-%m-%dT%H:%M:%S'))

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
--beautiful.init(awful.util.get_themes_dir() .. "default/theme.lua")
beautiful.init(os.getenv('HOME') .. '/.config/awesome/themes/moje/theme.lua')
beautiful.wallpaper = os.getenv('HOME') .. '/wallpapers/3840x2160/linux_debian_brand_logo_spiral.jpg'
beautiful.wallpaper = os.getenv('HOME') .. '/wallpapers/20180422_153840_HDR.jpg'
-- This is used later as the default terminal and editor to run.
terminal = "x-terminal-emulator"
editor = os.getenv("EDITOR") or "editor"
editor_cmd = terminal .. " -e " .. editor
browser = os.getenv("BROWSER") or "sensible-browser " or "luakit -u "
browser_blank = "luakit -u about:blank"

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"
alt_l = "Mod1"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier,
    awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Helper functions
local function client_menu_toggle_fn()
    local instance = nil

    return function ()
        if instance and instance.wibox.visible then
            instance:hide()
            instance = nil
        else
            instance = awful.menu.clients({ theme = { width = 250 } })
        end
    end
end
-- }}}

-- {{{ Menu
powermenu = {
   { "suspend", "/home/piotr/.local/bin/suspend-moje" },
   { "hibernate", "sudo /bin/systemctl hibernate" },
   { "reboot", "sudo shutdown -r now" },
   { "halt", "sudo shutdown -h now" }
}
tmuxmenu = {
   -- { "sar0", terminal .. " -name sar0 -e ssh sar0 -t tmux -2 attach" },
   { "sar0", terminal .. " -name sar0 -e mosh sar0 tmux attach" },
   { "saf0", terminal .. " -name saf0 -e mosh saf0 tmux attach" },
   { "smh0", terminal .. " -name smh0 -e ssh smh0 -t tmux -2 attach" },
   { "snp0", terminal .. " -name sna0 -e ssh snp0 -t tmux -2 attach" },
   { "sgh0", terminal .. " -name sgh0 -e ssh sgh0 -t tmux -2 attach" },
   { "localhost", terminal .. " -name localhost -e tmux -2 attach" },
}

appmenu = {
   { "GeeQie", "geeqie" },
   { "Thunar", "thunar" },
   { "Qalculate", "qalculate" },
   { "pavucontrol", "pavucontrol" },
   { "Kodi", "kodi" },
   { "Firefox", "firefox" },
   { "Chrome", "google-chrome" },
   { "Vivaldi", "vivaldi" },
}
monitormenu = {
   {"laptop + monitor", function()
        awful.util.spawn("xrandr --output DP-1-2-1 --off --output HDMI-1-2 --off --output VGA-1-1 --off --output DP-1-1 --off --output DP-1-2 --off --output eDP-1-1 --auto --output DP-1-1 --primary --auto")
        awesome.restart()
    end},
   {"tylko laptop", function()
        awful.util.spawn("xrandr --output DP-1 --off --output HDMI-1 --off --output VGA-1 --off --output DP2-1 --off --output DP2-2 --off --output DP2-3 --off --output eDP-1 --auto --primary")
        awesome.restart()
    end},
   {"tylko monitor", function()
        awful.util.spawn("xrandr --output eDP-1 --off --output VGA-1 --off --output DP2-1 --off --output DP2-2 --off --output DP2-3 --off --output HDMI-1 --off --output DP-1 --auto --primary")
        awesome.restart()
    end},
   {"projektor", function()
        --awful.util.spawn("xrandr --output HDMI-1 --auto --dpi 96 --above eDP-1")  -- thunderbolt→HDMI
        --awful.util.spawn("xrandr --output DP2-1 --auto --dpi 96 --above DP-1")  -- docking station
        awful.util.spawn("xrandr --output DP-1-2-1 --auto --dpi 96 --above DP-1-1")  -- docking station
        awesome.restart()
    end},
   {"DP-1: monitor", function()
       awful.util.spawn("xrandr --output DP-1 --auto --dpi 96 --rotate normal --right-of eDP-1")
        awesome.restart()
    end},
   {"VGA-1", function()
        awful.util.spawn("xrandr --output VGA-1 --dpi 96 --auto --rotate normal --right-of eDP-1")
        awesome.restart()
    end},
   {"ciemny ekran laptopa", "sudo /usr/libexec/gsd-backlight-helper /sys/class/backlight/nvidia_0/ 1" },
   {"jasny ekran laptopa", "sudo /usr/libexec/gsd-backlight-helper /sys/class/backlight/nvidia_0/ 100" },
   {"blank", "xset dpms force off" }, -- xset s blank
   --{"lock", "xautolock -locknow" },
   {"lock", "i3lock -d -c 000000" },
}
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() return false, hotkeys_popup.show_help end},
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end}
}

mymainmenu = awful.menu({ items = {
    { "tmux", tmuxmenu},
    { "awesome", myawesomemenu, beautiful.awesome_icon },
    { "Debian", debian.menu.Debian_menu.Debian },
    { "ulubione", appmenu},
    { "monitor", monitormenu},
    { "zasilanie", powermenu}
}})

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()
mytextclock = wibox.widget.textclock('%a %d. %b, %H:%M:%S', 1)
cal.register(mytextclock, "<b>%s</b>")

-- Create a wibox for each screen and add it
local taglist_buttons = awful.util.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() and c.first_tag then
                                                      c.first_tag:view_only()
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, client_menu_toggle_fn()),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

local wtpl = "<span color='white'>%s</span>"
local white = function(str) return string.format(wtpl, str) end
local create_progressbar = function()
    pb = awful.widget.progressbar()
    pb:set_width(21)
    pb:set_height(16)
    pb:set_vertical(true)
    pb:set_background_color("#494B4F")
    pb:set_border_color(beautiful.border_normal)
    pb:set_color("")
    pb:set_color({ type = "linear", from = { 0, 0 }, to = { 0, 20 }, stops = { { 0, "#AECF96" }, { 0.5, "#ACACAC" }, { 1, "#E1E1E1" } }})
    return pb
end

-- CPU
local cpuwidget = awful.widget.graph()
cpuwidget:set_width(100)
cpuwidget:set_height(16)
cpuwidget:set_border_color(beautiful.border_normal)
cpuwidget:set_background_color("#494B4F")
cpuwidget:set_color("#FF5656")
cpuwidget:set_color({ type = "linear", from = { 0, 0 }, to = { 0, 20 }, stops = { { 0, "#FF5656" }, { 0.5, "#ACACAC" }, { 1, "#E1E1E1" } }})
vicious.register(cpuwidget, vicious.widgets.cpu, "$1")

-- memory
local memwidget = create_progressbar()
vicious.register(memwidget, vicious.widgets.mem, "$1", 13)
mem2widget = wibox.widget.textbox()
vicious.cache(vicious.widgets.mem)
vicious.register(mem2widget, vicious.widgets.mem, "$4<small>M</small>", 13)

-- battery widget
local batwidget = create_progressbar()
vicious.register(batwidget, vicious.widgets.bat, "$2", 61, "BAT0")
bat2widget = wibox.widget.textbox()
vicious.register(bat2widget, vicious.widgets.bat, white('⚡') .. "$2<small>%</small>", 61, "BAT0")

-- battery tooltip
local b_tooltip = awful.tooltip({})
b_tooltip:add_to_object(bat2widget)

bat2widget:connect_signal("mouse::enter", function()
    awful.spawn.easy_async('acpi',
        function(stdout, stderr, reason, exit_code)
            b_tooltip:set_text(stdout)
        end)
    end
)

-- filesystem
--fswidget = wibox.widget.textbox()
--vicious.cache(vicious.widgets.fs)
--vicious.register(fswidget, vicious.widgets.fs,
--  white('ROOT:') .. " ${/ avail_gb}<small>G</small>  " ..
--  white('PRIV:') .. " ${/priv avail_gb}<small>G</small>  " ..
--  white('TMP:') .. " ${/tmp avail_gb}<small>G</small>  " ..
--  white('DANE:') .. " ${/dane avail_gb}<small>G</small>  ")

-- network
netwidget = wibox.widget.textbox()

local function read_file(path)
    local file = io.open(path, 'rb')
    if not file then return nil end
    local content = file:read '*a' -- *a or *all reads the whole file
    file:close()
    return content
end

if read_file('/sys/class/net/hub8in1/carrier') == '1\n' then
    net_dev = 'hub8in1'
elseif read_file('/sys/class/net/enp110s0/carrier') == '1\n' then
    net_dev = 'enp110s0'
elseif read_file('/sys/class/net/wlan0/carrier') == '1\n' then
    net_dev = 'wlan0'
else
    net_dev = 'eth0'
end

vicious.register(netwidget, vicious.widgets.net,
  white('') .. '${' .. net_dev .. ' up_kb}<small>k</small> '..
  white('') .. '${' .. net_dev .. ' down_kb}<small>k</small> ', 1)

awful.tooltip({objects={netwidget}, text=net_dev})

-- audio
local audio_card = 'alsa_output.pci-0000_00_1f.3.analog-stereo'  -- pactl list | grep alsa_output
pulsewidget = wibox.widget.textbox()
vicious.register(pulsewidget, vicious.contrib.pulse, white('♬') .. "$1<small>%</small>", 2, audio_card)
pulsewidget:buttons(awful.util.table.join(
  awful.button({}, 1, function() awful.util.spawn("pavucontrol") end),
  awful.button({}, 4, function() vicious.contrib.pulse.add(1, audio_card) end),
  awful.button({}, 5, function() vicious.contrib.pulse.add(-1, audio_card) end)
))

-- audio - progressbar
local audio_pb = create_progressbar()
vicious.register(audio_pb, vicious.contrib.pulse, "$1", 2, audio_card)
--audio_pb.buttons(awful.util.table.join(
--  awful.button({}, 1, function() awful.util.spawn("pavucontrol") end),
--  awful.button({}, 5, function() vicious.contrib.pulse.add(1, audio_card) end),
--  awful.button({}, 5, function() vicious.contrib.pulse.add(-1, audio_card) end)
--))

sec_screen = 2
primary_screen = 1
local layouts = awful.layout.layouts

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
    if s.index == primary_screen then
        awful.tag({"➊", "➋", "➌", "➍", "➎", "➏", "➐", "➑", "➒", "➓"}, s, {
            layouts[6], layouts[6], layouts[6],    -- Tags: 1, 2, 3
            layouts[6], layouts[3], layouts[10],   --       4, 5 ,6
            layouts[10], layouts[10], layouts[10], --       7, 8, 9
            layouts[10]
        })
    elseif s.index == sec_screen then
        awful.tag({"➊", "➋", "➌", "➍", "➎", "➏", "➐", "➑", "➒", "➓"}, s, {
            layouts[10], layouts[10], layouts[3],  -- Tags: 1, 2, 3
            layouts[10], layouts[10], layouts[6],  --       4, 5 ,6
            layouts[6], layouts[6], layouts[6],    --       7, 8, 9
            layouts[6]
        })
    else
        -- awful.tag({1, 2, 3, 4, 5, 6, 7, 8, 9 }, s, awful.layout.suit.tile.left)
        awful.tag({"➊", "➋", "➌", "➍", "➎", "➏", "➐", "➑", "➒", "➓"}, s, awful.layout.suit.tile.left)
    end

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            s.mytaglist,
            mylauncher,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            mykeyboardlayout,
            s.index == sec_screen and netwidget,
            -- s.index == primary_screen and fswidget,
            wibox.widget.systray(),
            s.index == primary_screen and cpuwidget,
            s.index == primary_screen and mem2widget,
            s.index == primary_screen and memwidget,
            s.index == primary_screen and bat2widget,
            s.index == primary_screen and batwidget,
            s.index == primary_screen and pulsewidget,
            s.index == primary_screen and audio_pb,
            mytextclock,
            s.mylayoutbox,
        },
    }
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

local select_window = function(idx)
    awful.client.focus.byidx(idx)
    if client.focus then client.focus:raise() end
end

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
              {description = "go back", group = "tag"}),

    awful.key({ modkey,           }, "j", awful.tag.viewprev ),
    awful.key({ modkey,           }, "k", awful.tag.viewnext ),
    awful.key({ modkey, alt_l     }, "j", function() select_window(-1) end),
    awful.key({ modkey, alt_l     }, "k", function() select_window( 1) end),
    awful.key({ modkey,           }, "Up",   function() select_window( 1) end),
    awful.key({ modkey,           }, "Down", function() select_window(-1) end),
    awful.key({ modkey,           }, "w", function () mymainmenu:show() end,
              {description = "show main menu", group = "awesome"}),
     -- application switcher
     awful.key({ modkey }, "q", function()
     -- If you want to always position the menu on the same place set coordinates
     awful.menu.menu_keys.down = { "Down", "Alt_L" }
     local cmenu = awful.menu.clients({width=256}, { keygrabber=true})
 end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey,           }, "`", function() awful.screen.focus_relative(1) end,
              {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
              {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey, alt_l          }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, alt_l, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),

    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                      client.focus = c
                      c:raise()
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Prompt
    awful.key({ modkey },            "r",     function () awful.screen.focused().mypromptbox:run() end,
              {description = "run prompt", group = "launcher"}),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"}),
    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end,
              {description = "show the menubar", group = "launcher"}),

    -- XF86
    awful.key({}, "XF86Mail", function() awful.util.spawn("mpc prev", false) end),
    awful.key({"Shift"}, "XF86Mail", function() awful.util.spawn("mpc next", false) end),
    awful.key({}, "XF86Calculator", function() awful.util.spawn("qalculate", false) end),
    awful.key({"Shift"}, "XF86Calculator", function() awful.util.spawn(terminal .. " -e ipython3", false) end),
    awful.key({}, "XF86HomePage", function() awful.util.spawn("mpc toggle", false) end),
    awful.key({}, "XF86AudioPlay", function() awful.util.spawn("mpc toggle", false) end),
    awful.key({}, "XF86AudioPrev", function() awful.util.spawn("mpc prev", false) end),
    awful.key({}, "XF86AudioNext", function() awful.util.spawn("mpc next", false) end),
    awful.key({}, "XF86AudioMute", function() awful.util.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle", false) end),
    awful.key({}, "XF86AudioLowerVolume", function() awful.util.spawn("pactl set-sink-volume @DEFAULT_SINK@ -2%", false) end),
    awful.key({}, "XF86AudioRaiseVolume", function() awful.util.spawn("pactl set-sink-volume @DEFAULT_SINK@ +2%", false) end),
    awful.key({}, "XF86Sleep", function()
        awful.util.spawn('/home/piotr/.local/bin/suspend-moje') end),
    awful.key({}, "XF86Suspend", function() awful.util.spawn('sudo /bin/systemctl hibernate') end),
    awful.key({}, "XF86MonBrightnessUp", function() awful.util.spawn('backlight +') end),
    awful.key({}, "XF86MonBrightnessDown", function() awful.util.spawn('backlight -') end),
    awful.key({}, "XF86TouchpadOff", function() awful.util.spawn('synclient TouchpadOff=1') end),
    awful.key({"Shift" }, "XF86TouchpadOff", function() awful.util.spawn('synclient TouchpadOff=0') end),
    awful.key({}, "XF86Battery", function()
        awful.util.spawn('notify-send "' .. awful.util.pread('acpi') .. '"')
    end),
    --awful.key({}, "XF86Display", ...)  -- fn-F4
    -- inne
    awful.key({"Control", "Alt_L", "Shift"}, "Print", function()
        awful.util.spawn('import /tmp/okienko_' .. os.date('%Y%m%d_%H%M%S') .. '.png', false)
    end), -- zrzut okna
    awful.key({"Control", "Alt_L"}, "Print", function()
        awful.util.spawn('import -w root /tmp/screen_' .. os.date('%Y%m%d_%H%M%S') .. '.png', false)
    end), -- zrzut ekranu
    awful.key({"Control", "Shift"}, "Escape", function() awful.util.spawn_with_shell("sleep 1; xset dpms force off") end),
    awful.key({modkey, "Control"}, "Delete", function()
        awful.util.spawn('/home/piotr/.local/bin/suspend-moje') end),

    -- google
    awful.key({ modkey, "Shift" }, "g", function() awful.util.spawn(
        browser .. ' "https://www.google.com/search?q=' .. selection() .. '"') end),
    awful.key({ modkey, "Shift" }, "t", function() awful.util.spawn(
        browser .. ' "https://translate.google.com/#auto|pl|' .. selection() .. '"') end),
    awful.key({ modkey, "Shift" }, "m", function() awful.util.spawn(
        browser .. ' "https://maps.google.pl/maps?q=' .. selection() .. '"') end),
    awful.key({ modkey, "Shift" }, "y", function() awful.util.spawn(
        browser .. ' "http://www.youtube.com/results?search_query=' .. selection() .. '"') end),
    -- waluty
    awful.key({ modkey, "Shift" }, "u", function() awful.util.spawn(
        browser .. ' "https://duckduckgo.com/?ia=currency&q=' .. selection() .. ' USD in PLN"') end),
    awful.key({ modkey, "Shift" }, "e", function() awful.util.spawn(
        browser .. ' "https://duckduckgo.com/?ia=currency&q=' .. selection() .. ' EUR in PLN"') end),
    awful.key({ modkey, "Shift" }, "f", function() awful.util.spawn(
        browser .. ' "https://duckduckgo.com/?ia=currency&q=' .. selection() .. ' GBP in PLN"') end),
    -- Debian
    awful.key({ modkey, "Shift" }, "b", function() awful.util.spawn(
        browser .. ' "http://bugs.debian.org/' .. selection() .. '"') end),
    awful.key({ modkey, "Shift" }, "p", function() awful.util.spawn(
        browser .. ' "https://tracker.debian.org/pkg/' .. selection() .. '"') end),
    -- zakupy
    awful.key({ modkey, "Shift" }, "c", function() awful.util.spawn(
        browser .. ' "http://www.ceneo.pl/;szukaj-' .. selection() .. '"') end),
    awful.key({ modkey, "Shift" }, "s", function() awful.util.spawn(
        browser .. ' "http://cenowarka.pl/?fs=' .. selection() .. '"') end),
    awful.key({ modkey, "Shift" }, "a", function() awful.util.spawn(
            browser .. ' "http://allegro.pl/listing/listing.php?string=' .. selection() .. '"') end),
    -- filmy
    awful.key({ modkey, "Shift" }, "i", function() awful.util.spawn(
        browser .. ' "http://www.imdb.com/find?q=' .. selection() .. '"') end),
    awful.key({ modkey, "Shift" }, "z", function() awful.util.spawn(
        --browser .. ' "https://search.torrents.io/' .. selection() .. ':zooqle/"') end),
        browser .. ' "https://zooqle.com/search?q=' .. selection() .. '"') end),
    -- inne
    awful.key({ modkey, "Shift" }, "d", function() awful.util.spawn(
        browser .. ' "https://duckduckgo.com/?t=debian&q=' .. selection() .. '"') end),
    awful.key({ modkey, "Shift" }, "w", function() awful.util.spawn(
        browser .. ' "http://pl.wikipedia.org/w/index.php?search=' .. selection() .. '"') end),
    awful.key({ modkey, "Shift" }, "o", function()
        awful.util.spawn(browser .. ' "' .. selection() .. '"') end),

    awful.key({modkey, "Control"}, "u", function()
        awful.util.spawn('/home/piotr/.local/bin/passmenu --type --user') end),
    awful.key({modkey, "Control"}, "e", function()
        awful.util.spawn('/home/piotr/.local/bin/passmenu --type --email') end),
    awful.key({modkey, "Control", "Shift"}, "o", function()
        awful.util.spawn('/home/piotr/.local/bin/passmenu --type --otp') end),
    awful.key({modkey, "Control"}, "p", function()
        awful.util.spawn('/home/piotr/.local/bin/passmenu --type') end)
)

local set_opacity = function(client, value)
    local v = client.opacity + value
    if v >= 1 then
        v = 1
    elseif v < 0.1 then
        v = 0.1
    end
    client.opacity = v
end

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey, "Shift"   }, "x",      function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
              {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"}),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "maximize", group = "client"}),
    -- przezroczystość
    awful.key({ modkey, }, "Next", function(c) set_opacity(c, -0.1) end),
    awful.key({ modkey, }, "Prior", function(c) set_opacity(c, 0.1) end)
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 10 do
    globalkeys = awful.util.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

--- Q: how to get window class? A: xprop | grep "WM_CLASS\|^WM_NAME"
--                              WM_CLASS(STRING) = "instance", "class"
--                              WM_NAME(STRING) = "name"
-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "QEMU",
          "Dialog",
        },
        class = {
            "vlc", "Display", "qemu.*", "kvm", "Gimp", "wmplayer.exe", "Wine",
            "Qalculate", "Xawtv", "Xdtv", "Meld", "Griffith", "Acroread-*",
            "mocp", "VirtualBox", "Xmessage", "Xephyr*", "xmame", "Pinentry",
            "pinentry", "Wpa_gui",
            "Wicd-client.py", 'Pnmixer', 'Flirc', 'MPlayer', 'mplayer2', 'mpv',
            "Find & Replace", 'Gitk', 'Git-gui', 'Gitg', 'mplayer', 'Rozmowy z *'
        },

        name = {
          "Event Tester",  -- xev.
          "wxGlade*",
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "dialog" }
      }, properties = { titlebars_enabled = true }
    },
    {rule_any = {
        class = {
            "Pavucontrol"},
        name = {
            "Otwórz plik", "Zapisz plik", "Otwórz", "Zapisz",
            "Open File", "Save File"}
    }, properties={floating=true, opacity=0.9}},
    {rule={class="Kodi"}, properties={fullscreen=true, floating=true, maximized_vertical=true, maximized_horizontal=true}},
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and
      not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = awful.util.table.join(
        awful.button({ }, 1, function()
            client.focus = c
            c:raise()
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            client.focus = c
            c:raise()
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
        and awful.client.focus.filter(c) then
        client.focus = c
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

-- battery warning
-- created by bpdp

local function trim(s)
  return s:find'^%s*$' and '' or s:match'^%s*(.*%S)'
end

local function bat_notification()

  local f_capacity = assert(io.open("/sys/class/power_supply/BAT0/capacity", "r"))
  local f_status = assert(io.open("/sys/class/power_supply/BAT0/status", "r"))

  local bat_capacity = tonumber(f_capacity:read("*all"))
  local bat_status = trim(f_status:read("*all"))

  if (bat_capacity <= 10 and bat_status == "Discharging") then
    naughty.notify({ title      = "Battery Warning"
      , text       = "Battery low! " .. bat_capacity .."%" .. " left!"
      , fg="#ff0000"
      , bg="#deb887"
      , timeout    = 15
      , position   = "bottom_left"
    })
  end
end

battimer = timer({timeout = 120})
battimer:connect_signal("timeout", bat_notification)
battimer:start()

-- end here for battery warning

-- vim: fdm=marker
